import type {Sex} from '@/models/Sex';

export interface Employee
{
    id: string
    name: string
    age: number
    sex: Sex
}

export interface EmployeeListResponse
{
    employees: Employee[]
    total: number
}

export interface GroupedEmployeesAgeStat {
    ageInterval: number
    frequency: number
}

export interface EmployeeAgeStatsResponse
{
    femaleStats: GroupedEmployeesAgeStat[]
    maleStats: GroupedEmployeesAgeStat[]
    totalStats: GroupedEmployeesAgeStat[]
}

export interface EmployeeUpdateRequest
{
    name: string
    age: number
    sex: Sex
}

export interface EmployeeCreatedResponse
{
    id: string
}
