import type {
    EmployeeAgeStatsResponse, EmployeeCreatedResponse,
    EmployeeListResponse,
    EmployeeUpdateRequest
} from "@/models/Employee";
import AxiosClient from "@/repositories/AxiosClient";
import type {AxiosResponse} from "axios";

export const employeeRepository = () => {
    return {
        loadEmployeeList: (): Promise<AxiosResponse<EmployeeListResponse>> => AxiosClient.get<EmployeeListResponse>('/employees'),
        loadAgeStats: (binSize: number): Promise<AxiosResponse<EmployeeAgeStatsResponse>> => AxiosClient.get<EmployeeAgeStatsResponse>(`/employees/age-stats/${binSize}`),
        removeEmployee: (id: string): Promise<AxiosResponse<void>> => AxiosClient.delete<void>(`/employees/${id}`),
        editEmployee: (id: string, employeeUpdateRequest: EmployeeUpdateRequest): Promise<AxiosResponse<void>> => AxiosClient.patch<void>(`/employees/${id}`, employeeUpdateRequest),
        addEmployee: (employeeUpdateRequest: EmployeeUpdateRequest): Promise<AxiosResponse<EmployeeCreatedResponse>> => AxiosClient.post<EmployeeCreatedResponse>('employees/', employeeUpdateRequest)
    }
}
