import axios from "axios";

const baseDomain = import.meta.env.VITE_API_URL;
const baseURL = `${baseDomain}`;

export default axios.create({
    baseURL,
});
