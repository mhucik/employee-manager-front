import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'

import './assets/sb-admin-2.css'
import '@/scss/styles.scss'

import { library } from "@fortawesome/fontawesome-svg-core";
import { faTachometerAlt, faUsers, faPencilAlt, faTrashAlt, faPlus, faCheck } from "@fortawesome/free-solid-svg-icons";

library.add(faTachometerAlt);
library.add(faUsers);
library.add(faPencilAlt);
library.add(faTrashAlt);
library.add(faPlus);
library.add(faCheck);
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


createApp(App)
    .component('faicon', FontAwesomeIcon)
    .use(router)
    .mount('#app')

import "bootstrap/dist/js/bootstrap.min.js";
import './assets/sb-admin-2.js'

