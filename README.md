## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```


## Project setup using Docker

### Setup environment

Edit or add `.env.local` file and setup API URL, if you need to run API on different address than is specified in `.env` file.

For example: 

```
VITE_API_URL=http://localhost:4567/api/v1
```

### Build Docker image
```sh
docker build . -t mhucik/employee-manager-front
```

### Run Docker container
```sh
docker run -p 4000:8080 mhucik/employee-manager-front
```

Don't forget to run [Employee Manager API](https://gitlab.com/mhucik/employee-manager) on address specified in `.env.local` file.


### DEMO

![Demo](demo.gif)
